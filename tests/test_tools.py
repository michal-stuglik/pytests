from tools import sum_items


class TestTools(object):
    def setup(self):
        self.x = 1
        self.y = 4

    def teardown(self):
        pass

    def test_sum_items(self):
        assert sum_items(self.x, self.y) == 5
